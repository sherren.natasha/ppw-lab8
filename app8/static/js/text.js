var counter = 0;
function changeStar(id){
  var star = $('#'+id).html();
  if(star.includes("gray")) {

    $.ajax({
      url:"/add",
      dataType: 'json',
      success: function(result){
        var counter = JSON.parse(result);
    $('#'+id).html("<i class='fa fa-star' style='color : green'></i>");
    $("#fav").html("<i class='fa fa-star'style='color : green'></i> " + counter + " ");


      }
    });

  }
  else{
    $.ajax({
      url:"/remove",
      dataType:'json',
      success: function(result){
        var counter = JSON.parse(result);

        $('#'+id).html("<i class='fa fa-star' style='color : gray'></i>");
      $("#fav").html("<i class='fa fa-star'style='color : green'></i> " + counter + " ");
      }
    });
      
  }   
}
$(document).ready(function(){
	cari("quilting");
});

$("#searchbtn").click(function(){
	var textbox = $("#searchbar").val();
	cari(textbox);
});


function cari(txt){
  $.ajax({
    url: "/jsonsearch?cari=" + txt,
    success: function(result){
      result = result.items;
      var header = "<thead><tr><th>Title</th> <th>Author</th> <th>Published Date</th> <th></th> </tr><head>";
      $("#result thead").remove();
      $("#result").append(header);
      $("#result tbody").remove();
      $("#result").append("<tbody>");

      for(i=0; i<result.length; i++){
        var tmp = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" + result[i].volumeInfo.authors + "</td><td>"+ result[i].volumeInfo.publishedDate +"</td><td>" +"<button class='button' style = 'background-color: Transparent; border: none' id='"+result[i].id+"' onclick = 'changeStar(" +"\""+result[i].id+"\""+")'><i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>";
        $("#result").append(tmp);
      }
      $("#result").append("</tbody>");
    }
  })
}
    
var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 2000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}
