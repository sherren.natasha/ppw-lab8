from django.urls import path
from django.contrib.auth import views
from django.conf import settings
from django.conf.urls import url, include
from .views import home, books, search, login,logout,tambah,kurang


urlpatterns = [
    path('', home, name = 'home'),
    path('jsonfile', books,name = 'books'),
    path('jsonsearch', search, name="search"),
    path('login', login, name = 'login'),
    path('logout', logout, name = 'logout'),
    path('add',tambah, name='add'),
    path('remove', kurang, name = 'remove'),
    
]
