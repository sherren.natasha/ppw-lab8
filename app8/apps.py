from django.apps import AppConfig


class Lab8AppsConfig(AppConfig):
    name = 'app8'
