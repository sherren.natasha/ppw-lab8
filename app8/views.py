import json
from django.shortcuts import render
from django.http import JsonResponse
import requests
from django.core import serializers
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout




def books(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
    data = requests.get(url).json()
    return JsonResponse(data)
    
def home(request):
        if request.user.is_authenticated:
            request.session['user'] = request.user.username
            request.session['email'] = request.user.email

            request.session.get('counter',0)
            print(dict(request.session))
            for key, value in request.session.items():
                print('{} => {}'.format(key,value))
        return render(request,'story9.html')

def search(request):
    searching =  request.GET.get('cari')	
    url = "https://www.googleapis.com/books/v1/volumes?q=" + searching
    data = requests.get(url).json()
    if request.user.is_authenticated :
        count = request.session.get('counter',0)
        request.session['counter'] = count
        for key, value in request.session.items():
            print('{} => {}'.format(key, value))
    return JsonResponse(data)

def tambah(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type = "application/json")

def kurang(request):

	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = "application/json")


def login(request):
    return render(request, 'login.html')

def logout(request):
	request.session.flush()
	#logout(request)
	return HttpResponseRedirect('/')
