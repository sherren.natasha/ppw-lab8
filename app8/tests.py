from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import books, home

# Create your tests here.


class Lab9UnitTest(TestCase):
	def test_lab_9_list_book_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_json_file_is_exist(self):
		response = Client().get('/jsonfile')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_using_books_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'story9.html')
