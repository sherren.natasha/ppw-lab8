from django import forms

class formRegistrasi(forms.Form):
    nama = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs={'class' : 'form-control',
                                                                                                     'placeholder' : 'Nama'}))
    email = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs={'class' : 'form-control',
                                                                                                       'type' : 'email',
                                                                                                       'placeholder' : 'Alamat Email'}))
    password = forms.CharField(label='', required=True, max_length=30,  widget=forms.TextInput(attrs={'class' : 'form-control',
                                                                                                              'type' : 'password',
                                                                                                              'placeholder' : 'Password'}))
