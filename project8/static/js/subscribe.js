var is_unique = false;
$(function() {
    $("#id_email").change(function () {
        console.log($(this).val());
        email = $(this).val();
        $.ajax({
            method: "POST",
            url: '{% url "app8:validate_email" %}',
            data: {
                'email': email
            },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (data.is_taken) {
                    alert("The email has already used. Please use another one!");
                    is_unique = false;
                } else {
                    is_unique = true;
                }
            }
        });
    });

    $('#id_nama').on('keyup', function () {
        check_submit_button();
    });
    $('#id_email').on('keyup', function () {
        check_submit_button();
    });
    $('#id_password').on('keyup', function () {
        check_submit_button();
    });

});



function check_submit_button(){
    var panjang_nama = $('#id_nama').val().length;
    var panjang_email = $('#id_email').val().length;
    var panjang_password = $('#id_password').val().length;

    if(panjang_nama > 0 && panjang_email > 0 && panjang_password > 0 && is_unique) {
        $('#btn_submit').prop('disabled', false);
    } else {
        $('#btn_submit').prop('disabled', true);
    }
}

function submit_post() {
    console.log("create post is working!"); 
    $.ajax({
        url : "/registrasi_post/", 
        type : "POST", 
        data : { nama : $('#id_nama').val(), email : $('#id_email').val(), password : $('#id_password').val() }, // data sent with the post request

        success : function(json) {
            $('#id_nama').val(''); 
            $('#id_email').val(''); 
            $('#id_password').val(''); 

            console.log(json); 
            console.log("success");
            
            $('#btn_submit').prop('disabled', true);
        },
    });
}


